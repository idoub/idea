﻿var leftColumn = document.getElementById('leftColumn');
var input = document.getElementById('input');
var container = document.getElementById('container');
var inset = document.getElementById('inset');
var resizeBar = document.getElementById('resizeBar');

var camera, controls, scene, renderer;
var camera2, scene2, renderer2;
var zoomingIn, zoomingOut, panningLeft, panningRight;
var clock = new THREE.Clock();

var targetWindow;
var separator = 400;

var animator = new Array();

var CONTROL = false;

/* COLORS */
var WHITE = 0xFFFFFF;
var SILVER = 0xC0C0C0;
var GRAY = 0x808080;
var BLACK = 0x000000;
var RED = 0xFF0000;
var MAROON = 0x800000;
var YELLOW = 0xFFFF00;
var OLIVE = 0x808000;
var LIME = 0x00FF00;
var GREEN = 0x008000;
var AQUA = 0x00FFFF;
var TEAL = 0x008080;
var BLUE = 0x0000FF;
var NAVY = 0x000080;
var FUCHSIA = 0xFF00FF;
var PURPLE = 0x800080;

/******************************************************************************/
/**                                  EVENTS                                  **/
/******************************************************************************/
function onKeyDown( e ) {
    if(targetWindow == container) {
        e.preventDefault();
    } else {
        return;
    }
    switch(e.which) {
        case 65: // a
            panningLeft = true;
            break;
        case 68: // d
            panningRight = true;
            break;
        case 83: // s
            zoomingOut = true;
            break;
        case 87: // w
            zoomingIn = true;
            break;
    }
}

function onKeyUp( e ) {
    if(targetWindow == container) {
        e.preventDefault();
    } else {
        return;
    }
    switch(e.which) {
        case 65: // a
            panningLeft = false;
            break;
        case 68: // d
            panningRight = false;
            break;
        case 83: // s
            zoomingOut = false;
            break;
        case 87: // w
            zoomingIn = false;
            break;
    }
}

function onMouseOver( e ) {
    e.stopPropagation();
    targetWindow = e.currentTarget
    targetWindow.focus();
}

function onInputDown( e ) {
    if(CONTROL) e.preventDefault();
    switch(e.which) {
        case 9: // tab
            e.preventDefault();
            document.execCommand('indent',false,null);
            break;
        case 17: // ctrl
            CONTROL = true;
            break;
        case 83: // s
            writeCookie();
    }
}

function onInputUp( e ) {
    switch(e.which) {
        case 17: // ctrl
            CONTROL = false;
            break;
    }
}

function onResizePress( e ) {
    e.preventDefault();
    document.addEventListener( 'mousemove', onResizeWidth );
    document.addEventListener( 'mouseup', onResizeRelease );
}

function onResizeWidth( e ) {
    separator = e.pageX;
    onWindowResize( e );
}

function onResizeRelease( e ) {
    document.removeEventListener( 'mousemove', onResizeWidth );
    document.removeEventListener( 'mouseup', onResizeRelease );
}

function onWindowResize( e ) {
    var width = window.innerWidth - separator - 4;
    var height = window.innerHeight - 4;
    
    leftColumn.style.width = separator - 6;
    leftColumn.style.height = height;
    input.style.height = window.innerHeight - 78;
    
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize( width, height );
    render();
}

function onHelpOpen( e ) {
    document.getElementById( 'help' ).style.display = 'block';
    document.getElementById( 'closeHelp' ).addEventListener( 'click', onHelpClose);
}

function onHelpClose( e ) {
    document.getElementById( 'closeHelp' ).removeEventListener( 'click', onHelpClose);
    document.getElementById( 'help' ).style.display = 'none';
}

/******************************************************************************/
/**                                  OBJECTS                                 **/
/******************************************************************************/

/** SCENE OBJECTS **/
/* BALL */
var Ball = function( x, y, z, d, segWidth, segHeight, phiS, phiE, thetaS, thetaE ) {
    x = x || 0;
    y = y || 0;
    z = z || 0;
    d = d || 1;
    var geometry = new THREE.SphereGeometry ( d*0.5, segWidth, segHeight, phiS, phiE, thetaS, thetaE )
    var material = new THREE.MeshLamberMaterial( { color: 0xFF0000, side: THREE.DoubleSide } );
    var obj = new THREE.Mesh( geometry, material );
    obj.position.set( x, y, z );
    scene.add(obj);
    return obj;
}

/* BOX */
var Box = function( x, y, z, w, h, d ) {
    x = x || 0;
    y = y || 0;
    z = z || 0;
    w = w || 1;
    h = h || 1;
    d = d || 1;
    var geometry = new THREE.CubeGeometry( w, h, d );
    var material = new THREE.MeshLambertMaterial( { color: 0xFF0000, side: THREE.DoubleSide } );
    var obj = new THREE.Mesh( geometry, material );
    obj.position.set( x, y, z );
    scene.add(obj);
    return obj;
}

/* CONE */
var Cone = function( x, y, z, h, d, radSeg, heightSeg, open ) {
    x = x || 0;
    y = y || 0;
    z = z || 0;
    h = h || 1;
    d = d || 1;
    var geometry = new THREE.CylinderGeometry( 0, d*0.5, h, radSeg, heightSeg, open );
    var material = new THREE.MeshLambertMaterial( { color: 0xFF0000, side: THREE.DoubleSide } );
    var obj = new THREE.Mesh( geometry, material );
    obj.position.set( x, y, z );
    scene.add(obj);
    return obj;
}

/* CYLINDER */
var Cylinder = function( x, y, z, h, topD, botD, radSeg, heightSeg, open ) {
    x = x || 0;
    y = y || 0;
    z = z || 0;
    h = h || 1;
    topD = topD || 1;
    botD = botD || 1;
    var geometry = new THREE.CylinderGeometry( topD*0.5, botD*0.5, h, radSeg, heightSeg, open );
    var material = new THREE.MeshLambertMaterial( { color: 0xFF0000, side: THREE.DoubleSide } );
    var obj = new THREE.Mesh( geometry, material );
    obj.position.set( x, y, z );
    scene.add(obj);
    return obj;
}

/* PLANE */
var Plane = function( x, y, z, w, h, wSeg, hSeg, orientation ) {
    x = x || 0;
    y = y || 0;
    z = z || 0;
    w = w || 10;
    h = h || 10;
    if(typeof orientation != "string" ) orientation = "XZ";
    orientation.toUpperCase();
    var geometry = new THREE.PlaneGeometry( w, h, wSeg, hSeg );
    var material = new THREE.MeshLambertMaterial( { color: 0xFF0000, side: THREE.DoubleSide } );
    var obj = new THREE.Mesh( geometry, material );
    obj.position.set( x, y, z );
    switch(orientation) {
        case "XZ": case "ZX":
            obj.rotation.x = Math.PI * 0.5;
            break;
        case "ZY": case "YZ":
            obj.rotation.y = Math.PI * 0.5;
            break;
    }
    scene.add(obj);
    return obj;
}

/* PYRAMID */
var Pyramid = function( x, y, z, base, height, sides ) {
    x = x || 0;
    y = y || 0;
    z = z || 0;
    base = base || 1;
    height = height || 1;
    sides = Math.max(3, sides || 4 );
    var geometry = new THREE.CylinderGeometry( 0, base*0.5, height, sides, 1 );
    var material = new THREE.MeshLambertMaterial( { color: 0xFF0000, side: THREE.DoubleSide } );
    var obj = new THREE.Mesh( geometry, material );
    obj.position.set( x, y, z );
    scene.add(obj);
    return obj;
}

/* TORUS */
var Torus = function( x, y, z, outerDiameter, innerDiameter, radialSegments, cylindricalSegments ) {
    x = x || 0;
    y = y || 0;
    z = z || 0;
    outerDiameter = outerDiameter || 2;
    innerDiameter = innerDiameter || 1;
    radialSegments = radialSegments || 12;
    cylindricalSegments = cylindricalSegments || 12;
    if(innerDiameter >= outerDiameter) {
        alert("The outer diameter of a torus must be greater than the inner diameter.");
        return 0;
    }
    var d = outerDiameter - innerDiameter;
    var geometry = new THREE.TorusGeometry( outerDiameter, d, radialSegments, cylindricalSegments );
    var material = new THREE.MeshLambertMaterial( { color: 0xFF0000, side: THREE.DoubleSide } );
    var obj = new THREE.Mesh( geometry, material );
    obj.position.set( x, y, z );
    obj.rotation.x = Math.PI * 0.5;
    scene.add(obj);
    return obj;
}

/** INNACCESSIBLE OBJECTS **/
/* GRID */
function makeGrid() {
    var gridGeometry = new THREE.Geometry();
    var i;
    for(i=-50; i<51; i=i+2) {
        gridGeometry.vertices.push( new THREE.Vector3( i, 0, -50 ) );
        gridGeometry.vertices.push( new THREE.Vector3( i, 0,  50 ) );
        gridGeometry.vertices.push( new THREE.Vector3( -50, 0, i ) );
        gridGeometry.vertices.push( new THREE.Vector3(  50, 0, i ) );
    }
    var gridMaterial = new THREE.LineBasicMaterial( { color: 0xBBBBBB } );
    var grid = new THREE.Line(gridGeometry, gridMaterial, THREE.LinePieces);
    scene.add( grid );
}

/* COORDINATE ARROWS */
function makeCoordinateArrows() {
    var coordinateArrows = new THREE.Object3D();
    var org = new THREE.Vector3( 0, 0, 0);
    
    var dir = new THREE.Vector3( 0, 0, 1 );
    coordinateArrows.add( new THREE.ArrowHelper( dir, org, 8, 0x0000FF ) ); // Blue = z
    dir = new THREE.Vector3( 0, 1, 0 );
    coordinateArrows.add( new THREE.ArrowHelper( dir, org, 8, 0x00FF00 ) ); // Green = y
    dir = new THREE.Vector3( 1, 0, 0 );
    coordinateArrows.add( new THREE.ArrowHelper( dir, org, 8, 0xFF0000 ) ); // Red = x
    
    return coordinateArrows;
}

/******************************************************************************/
/**                             OBJECT PROPERTIES                            **/
/******************************************************************************/

/* X */
Object.defineProperty(THREE.Mesh.prototype, 'x', {
    get: function () {
        return this.position.x;
    },
    set: function ( val ) {
        this.position.x = val;
    }
});

/* Y */
Object.defineProperty(THREE.Mesh.prototype, 'y', {
    get: function () {
        return this.position.y;
    },
    set: function ( val ) {
        this.position.y = val;
    }
});

/* Z */
Object.defineProperty(THREE.Mesh.prototype, 'z', {
    get: function () {
        return this.position.z;
    },
    set: function ( val ) {
        this.position.z = val;
    }
});

/* WIDTH */
Object.defineProperty(THREE.Mesh.prototype, 'w', {
    get: function() {
        return this.scale.x;
    },
    set: function( val ) {
        this.scale.x = val;
    }
});

/* HEIGHT */
Object.defineProperty(THREE.Mesh.prototype, 'h', {
    get: function() {
        return this.scale.y;
    },
    set: function( val ) {
        this.scale.y = val;
    }
});

/* LENGTH */
Object.defineProperty(THREE.Mesh.prototype, 'd', {
    get: function() {
        return this.scale.z;
    },
    set: function( val ) {
        this.scale.z = val;
    }
});

/* ROTATION X */
Object.defineProperty(THREE.Mesh.prototype, 'rotationX', {
    get: function() {
        return this.rotation.x;
    },
    set: function( val ) {
        this.rotation.x = val;
    }
});

/* ROTATION Y */
Object.defineProperty(THREE.Mesh.prototype, 'rotationY', {
    get: function() {
        return this.rotation.y;
    },
    set: function( val ) {
        this.rotation.y = val;
    }
});

/* ROTATION Z */
Object.defineProperty(THREE.Mesh.prototype, 'rotationZ', {
    get: function() {
        return this.rotation.z;
    },
    set: function( val ) {
        this.rotation.z = val;
    }
});

/* COLOR */
Object.defineProperty(THREE.Mesh.prototype, 'color', {
    get: function () {
        return this.material.color.getHexString();
    },
    set: function ( val ) {
        if(typeof val == 'string') val = parseInt( val );
        var c = new THREE.Color( val );
        this.material.color = c;
    }
});

/* TEXTURE */

/* OPACITY */
Object.defineProperty(THREE.Mesh.prototype, 'opacity', {
    get: function() {
        return this.material.opacity;
    },
    set: function( val ) {
        this.material.opacity = val;
        if( this.material.opacity < 1 ) this.material.transparent = true;
    }
});

/******************************************************************************/
/**                                CODE PARSER                               **/
/******************************************************************************/
function runCode() {
    var string = input.innerText;
    var returned = hljs.highlight('javascript', string);
    input.innerHTML = returned.value;
    
    scene.clear();
    makeGrid();
    eval(input.innerText);
    render();
}

/******************************************************************************/
/**                                 FUNCTION                                 **/
/******************************************************************************/
function render() {
    renderer.render( scene, camera );
    renderer2.render( scene2, camera2 );
}

function animate() {
    requestAnimationFrame( animate );
    controls.update( clock.getDelta() );
    
	camera2.position.copy( camera.position );
    camera2.position.sub( controls.center );
	camera2.position.setLength( 18 );
    camera2.lookAt( scene2.position );
    
    keyboardUpdate();
}

function keyboardUpdate() {
    if(zoomingIn) controls.zoomOut();
    if(zoomingOut) controls.zoomIn();
    if(panningLeft) controls.pan( new THREE.Vector3( -1, 0, 0 ));
    if(panningRight) controls.pan( new THREE.Vector3( 1, 0, 0 ));
}

function download() {
    var out = 'data:,' + encodeURI(input.innerText);
    downloadWithName(out, 'filename.idea');
}

function writeCookie() {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + 10);
    document.cookie='code=' + escape(input.innerText) + '; expires=' + exdate.toUTCString();
}

function clearInput() {
    input.innerHTML = "";
}

function resetInput() {
    document.cookie='code=deleted; expires=' + new Date(0).toUTCString();
    input.innerText = defaultInput();
    runCode();
}

function defaultInput() {
    var codeText = '';
    var c = document.cookie;
    if(c.length > 0) {
        codeText = unescape(c.substring(5,c.length));
    } else {
        codeText += '// The most basic example below has already been rendered.\n\n';
        codeText += 'var Cube1 = Box();\n\n\n';
        codeText += '// As an example of how to make stairs, uncomment the following code.\n\n';
        codeText += '//for(var i=1; i<=10; i++) {\n';
        codeText += '//\tBox(0,i*0.5,-i,10,i,1);\n';
        codeText += '//}';
    }
    return codeText;
}

/******************************************************************************/
/**                                   MAIN                                   **/
/******************************************************************************/
function init() {
    var width = window.innerWidth - separator - 6;
    var height = window.innerHeight - 4;
    
    /* Renderer */
    renderer = new THREE.WebGLRenderer();
    renderer.setSize( width, height );
    container.appendChild( renderer.domElement );
    
    /* Camera */
    camera = new THREE.PerspectiveCamera( 60, width / height, 1, 1000 );
    camera.position.set( 5, 5, 5 );
    camera.lookAt( 0, 0, 0 );
    
    /* Controls */
    controls = new THREE.OrbitControls( camera, container );
    controls.userPanSpeed = 0.2;
    
    /* Scene */
    scene = new THREE.Scene();
    scene.add(camera);
    makeGrid();
    
    /* Lights */
    light1 = new THREE.PointLight( 0xFFFFFF );
    light1.position.set( 100, 70, 40 );
    scene.add( light1 );
    light1 = new THREE.PointLight( 0xFFFFAA );
    light1.position.set( -100, -70, -40 );
    scene.add( light1 );
    
    /* Listeners */
    controls.addEventListener( 'change', render );
    container.addEventListener( 'keydown', onKeyDown );
    container.addEventListener( 'keyup', onKeyUp );
    container.addEventListener( 'mouseover', onMouseOver, false );
    input.addEventListener( 'mouseover', onMouseOver, false );
    input.addEventListener( 'keydown', onInputDown );
    input.addEventListener( 'keyup', onInputUp );
    resizeBar.addEventListener( 'mousedown', onResizePress );
    window.addEventListener( 'resize', onWindowResize );
    document.getElementById( 'helpButton' ).addEventListener( 'click', onHelpOpen );
    
    /* Input */
    leftColumn.style.width = separator - 6;
    leftColumn.style.height = window.innerHeight - 4;
    input.style.height = window.innerHeight - 78;
    input.contentEditable = true;
    
    /* Initial Code */
    input.innerText = defaultInput();
    
    /* Inset */
    renderer2 = new THREE.WebGLRenderer();
    renderer2.setSize( 140, 140 );
    inset.appendChild( renderer2.domElement );
    scene2 = new THREE.Scene();
    camera2 = new THREE.PerspectiveCamera( 60, 1, 1, 1000 );
    camera2.up = camera.up;
    scene2.add( camera2 );
    scene2.add( makeCoordinateArrows() );
    
    runCode();
}